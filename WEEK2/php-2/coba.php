<?php
    function pagar_bintang($integer){
        $temp = 1;
        while ($temp <= $integer){
            $r = 1;
            if ($temp % 2 == 0 ){
                while ($r <= $integer){
                    echo "* ";
                    $r++;
                }
                echo "<br>";
            } else {
                while ($r <= $integer){
                    echo "# ";
                    $r++;
                }
                echo "<br>";
            }
            $temp++;
            
        } 
        echo "<br>";
    }

    function xo($str){
        $temp1 = substr_count($str, "x");
        $temp2 = substr_count($str, "o");
        if ($temp1 == $temp2){
            echo $str . ': true';
        } else {
            echo $str . ': false';
        }
        echo "<br>";
    }

    echo xo('xoxoxo'); // "Benar"
    echo xo('oxooxo'); // "Salah"
    echo xo('oxo'); // "Salah"
    echo xo('xxooox'); // "Benar"
    echo xo('xoxooxxo'); // "Benar"
?>