<?php

require 'animal.php';
require 'frog.php';
require 'ape.php';

echo "<h2>Release 0</h2>";
$sheep = new Animal("shaun");

echo "Name : $sheep->name <br>"; 
echo "Legs : $sheep->legs <br>"; 
echo "Cold Blooded : $sheep->cold_blooded <br>"; 

echo "<h2>Release 1</h2>";
$kodok = new Frog("buduk");
echo "<br> Name: $kodok->name <br>";
echo "legs: $kodok->legs <br>"; 
echo "cold blooded: $kodok->cold_blooded <br>"; 
echo "Jump:  " . $kodok->jump();
    
$sungokong = new Ape("kera sakti");
echo "<br><br> Name: $sungokong->name <br>"; 
echo "legs: $sungokong->legs <br>";
echo "cold blooded: $sungokong->cold_blooded <br>"; 
echo "Yell: " . $sungokong->yell();
?>